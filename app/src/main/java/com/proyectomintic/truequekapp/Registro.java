package com.proyectomintic.truequekapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import com.proyectomintic.truequekapp.view.Articulos.Articulos;


public class Registro extends AppCompatActivity {

    Button btn_Registrateiniciosesion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);


        btn_Registrateiniciosesion = findViewById(R.id.btn_registrateiniciosesion);
        btn_Registrateiniciosesion.setOnClickListener(v -> {
            // Validar que los campos esten llenos
            // Enviar a validar el inicio de sesion

            // Abrir la ventana principal
            Intent intent = new Intent(Registro.this, Articulos.class);
            startActivity(intent);
        });
    }
}