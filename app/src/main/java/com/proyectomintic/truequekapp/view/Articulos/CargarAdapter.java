package com.proyectomintic.truequekapp.view.Articulos;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;
import androidx.annotation.NonNull;

import com.google.android.material.textview.MaterialTextView;
import com.proyectomintic.truequekapp.R;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;

public class CargarAdapter extends RecyclerView.Adapter<CargarAdapter.ViewHolder>  {
    List<CargarInfo> data;
    private OnItemClickListener onItemClickListener;
    public CargarAdapter(List<CargarInfo> data){
        this.data=data;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public CargarAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext())
                .inflate(R.layout.articulos_carga,parent,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull  CargarAdapter.ViewHolder holder, int position) {
        holder.cargarInformacion(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        MaterialTextView tvTitulo;
        MaterialTextView tvDescripcion;
        ImageView ivImagen;
        MaterialTextView tvEstado;

        private CargarInfo info;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTitulo=itemView.findViewById(R.id.tv_tituloArticulo);
            tvDescripcion=itemView.findViewById(R.id.tv_descripccionArticulo);
            ivImagen=itemView.findViewById(R.id.iv_imagenArticulo);
            tvEstado=itemView.findViewById(R.id.estado);

            if (onItemClickListener!=null){
                itemView.setOnClickListener(this);
            }

        }


        @Override
        public void onClick(View v) {
            if (onItemClickListener!=null){
                onItemClickListener.onItemClick(info);
            }
        }

        public void cargarInformacion(CargarInfo info){
            this.info=info;
            tvTitulo.setText(info.getNombre());
            tvDescripcion.setText(info.getDescripcion());
            ivImagen.setImageBitmap(convertirStringABitmap(info.getPath()));
            if (info.getAceptado()!=null){
                if (info.getAceptado()==true){
                    tvEstado.setText("Trueque Aceptado");
                    tvEstado.setTextColor(Color.parseColor("#2ECDD3"));
                }else if (info.getAceptado()==false){
                    tvEstado.setText("Trueque Rechazado");
                    tvEstado.setTextColor(Color.parseColor("#F57E7E"));
                }
            }

        }


        public Bitmap convertirStringABitmap(String path){
            InputStream stream = new ByteArrayInputStream(Base64.decode(path.getBytes(), Base64.DEFAULT));
            Bitmap bitmap= BitmapFactory.decodeStream(stream);
            return bitmap;
        }
        
    }
    public interface OnItemClickListener{
        void onItemClick(CargarInfo info);
    }
}
