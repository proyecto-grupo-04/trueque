package com.proyectomintic.truequekapp.view.Check;


import androidx.appcompat.app.AppCompatActivity;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.proyectomintic.truequekapp.R;
import com.proyectomintic.truequekapp.view.Articulos.Articulos;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

public class Check extends AppCompatActivity implements CheckMVP.View {

    public static final String EXTRA_ID = "id";
    public static final String EXTRA_TITULO = "titulo";
    public static final String EXTRA_DESCRIPCION = "descripcion";
    public static final String EXTRA_IMAGEN = "path";

    FloatingActionButton aceptar;
    FloatingActionButton rechazar;

    private CheckMVP.Presenter presenter;

    Integer id;
    TextView nombre;
    TextView descripcion;
    ImageView imagen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check);
        inicializar();
    }

    private void inicializar() {
        presenter=new CheckPresenter(this);

        nombre=findViewById(R.id.tv_tituloArticulo);
        descripcion=findViewById(R.id.tv_descripccionArticulo);
        imagen=findViewById(R.id.iv_imagenArticulo);

        id=Integer.parseInt(getIntent().getStringExtra(EXTRA_ID));
        nombre.setText(getIntent().getStringExtra(EXTRA_TITULO));
        descripcion.setText(getIntent().getStringExtra(EXTRA_DESCRIPCION));

        InputStream stream = new ByteArrayInputStream(Base64.decode(getIntent().getStringExtra(EXTRA_IMAGEN).getBytes(), Base64.DEFAULT));
        Bitmap bitmap= BitmapFactory.decodeStream(stream);
        imagen.setImageBitmap(bitmap);

        aceptar=findViewById(R.id.btn_aceptar);
        aceptar.setOnClickListener(v->{
            presenter.AceptarONo(obtenerNombre(),true);
            cambiarActividad(Articulos.class);
        });


        rechazar=findViewById(R.id.btn_rechazar);
        rechazar.setOnClickListener(v->{
            presenter.AceptarONo(obtenerNombre(),false);
            cambiarActividad(Articulos.class);
        });



    }

    @Override
    public String obtenerNombre() {
        return (String) nombre.getText();
    }

    @Override
    public void cambiarActividad(Class<? extends Activity> type) {
        startActivity(new Intent(this,type));
    }
}