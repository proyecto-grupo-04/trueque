package com.proyectomintic.truequekapp.view.Articulos;

import com.proyectomintic.truequekapp.model.repository.ArticuloRepository;
import com.proyectomintic.truequekapp.view.Check.Check;
import com.proyectomintic.truequekapp.view.NuevoArticulo.NuevoArticulo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ArticulosPresenter implements ArticulosMVP.Presenter {
    private final ArticulosMVP.View view;
    private final ArticulosMVP.Model model;

    public ArticulosPresenter(ArticulosMVP.View view) {
        this.view = view;
        this.model= new ArticuloRepository(view.getApplicationContext());
    }

    @Override
    public void cargarArticulos() {
        List<CargarInfo> articulos=model.obtenerArticulos();
        view.cargarArticulos(articulos);
    }

    @Override
    public void nuevoArticulo() {
        view.cambiarActividad(NuevoArticulo.class, null);
    }


    @Override
    public void abrirArticulo(CargarInfo info) {
        Map<String, String> parametros = new HashMap<>();
        parametros.put(Check.EXTRA_ID, info.getId().toString());
        parametros.put(Check.EXTRA_TITULO, info.getNombre());
        parametros.put(Check.EXTRA_DESCRIPCION, info.getDescripcion());
        parametros.put(Check.EXTRA_IMAGEN, info.getPath());

        view.cambiarActividad(Check.class, parametros);
    }
}
