package com.proyectomintic.truequekapp.view.Articulos;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.proyectomintic.truequekapp.InicioSesion;
import com.proyectomintic.truequekapp.R;
import com.proyectomintic.truequekapp.view.Check.Check;
import java.util.List;
import java.util.Map;

public class Articulos extends AppCompatActivity implements ArticulosMVP.View {
    FloatingActionButton btnnuevoArticulo;
    RecyclerView rvListaArticulos;
    ArticulosMVP.Presenter presenter;
    ImageButton ibtnSalir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_articulos);
        
        inicializar();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.cargarArticulos();
    }

    private void inicializar() {
        presenter=new ArticulosPresenter(this);
        btnnuevoArticulo =findViewById(R.id.btn_nuevoarticulo);
        btnnuevoArticulo.setOnClickListener(v->{
            presenter.nuevoArticulo();
        });

        ibtnSalir=findViewById(R.id.btn_Salir);
        ibtnSalir.setOnClickListener(v->{
            cambiarActividad(InicioSesion.class,null);
            
         });

        rvListaArticulos=findViewById(R.id.rv_listaArticulos);
        rvListaArticulos.setLayoutManager(new LinearLayoutManager(this));
    }


    @Override
    public void cargarArticulos(List<CargarInfo> articulos) {
        rvListaArticulos.setAdapter(new CargarAdapter(articulos));
        CargarAdapter adapter=new CargarAdapter(articulos);
        adapter.setOnItemClickListener(info -> {
            presenter.abrirArticulo(info);
        });
        rvListaArticulos.setAdapter(adapter);

    }

    public void onSalesClick(View view){
        presenter.nuevoArticulo();
    }

    @Override
    public void cambiarActividad(Class<? extends Activity> type, Map<String, String> parametros) {
        Intent cambiarActividadIntento= new Intent(this,type);
        if(parametros != null){
            for(String key : parametros.keySet()){
                cambiarActividadIntento.putExtra(key, parametros.get(key));
            }
        }

        startActivity(cambiarActividadIntento);
    }
}