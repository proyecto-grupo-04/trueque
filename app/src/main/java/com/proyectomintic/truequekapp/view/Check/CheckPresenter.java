package com.proyectomintic.truequekapp.view.Check;

import com.proyectomintic.truequekapp.model.repository.ArticuloRepository;

public class CheckPresenter implements CheckMVP.Presenter{
    private CheckMVP.Model model;
    private CheckMVP.View view;

    public CheckPresenter(CheckMVP.View view) {
        this.view=view;
        this.model=new ArticuloRepository(null);
    }

    @Override
    public void AceptarONo(String Nombre, Boolean decision) {
        model.AceptarONo(Nombre,decision);
    }
}
