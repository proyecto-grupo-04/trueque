package com.proyectomintic.truequekapp.view.Check;

import android.app.Activity;

public interface CheckMVP {
    interface Model{
        public void AceptarONo(String Nombre,Boolean decision);
    }

    interface View {
        String obtenerNombre();
        void cambiarActividad(Class<? extends Activity> type);
    }

    interface Presenter{
        void AceptarONo(String Nombre,Boolean decision);
    }
}
