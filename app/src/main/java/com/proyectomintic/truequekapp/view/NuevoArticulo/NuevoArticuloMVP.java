package com.proyectomintic.truequekapp.view.NuevoArticulo;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;

import java.util.List;

public interface NuevoArticuloMVP {
    interface Model{
        void guardarNuevoArticulo(NuevoArticuloInfo articulo);

    }
    interface Presenter{
        public void publicar();
        public String convertirBitmapAString(Bitmap bitmap);

    }
    interface View{
        void mostarMensaje(String mensaje);
        NuevoArticuloInfo getNuevoArticuloInfo();
        void cerrar();
        void cambiarActividad(Class<? extends Activity> type);
        void nombreIncorrecto(String mensaje);
        void descripcionIncorrecta(String mensaje);
        void mensajeError(String mensaje);
        Context getApplicationContext();

    }
}
