package com.proyectomintic.truequekapp.view.Articulos;

import android.app.Activity;

public class CargarInfo {
    private Integer id;
    private String Nombre;
    private String path;
    private String Descripcion;
    private Boolean Aceptado;

    public CargarInfo(Integer id, String nombre, String path, String descripcion,Boolean aceptado) {
        this.id=id;
        this.Nombre = nombre;
        this.path = path;
        this.Descripcion = descripcion;
        this.Aceptado=aceptado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getAceptado() {
        return Aceptado;
    }

    public void setAceptado(Boolean aceptado) {
        Aceptado = aceptado;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

}
