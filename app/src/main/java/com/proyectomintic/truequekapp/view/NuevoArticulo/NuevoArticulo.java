
package com.proyectomintic.truequekapp.view.NuevoArticulo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.proyectomintic.truequekapp.InicioSesion;
import com.proyectomintic.truequekapp.R;
import com.proyectomintic.truequekapp.view.Articulos.Articulos;
import com.proyectomintic.truequekapp.view.SubirImagen.SubirImagen;

import java.util.List;

public class NuevoArticulo extends AppCompatActivity implements NuevoArticuloMVP.View{
    private NuevoArticuloPresenter presenter;

    static final String Nombre="Nombre";
    static final String Descripcion="Descripcion";


    TextInputLayout tilNombreArticulo;
    TextInputLayout tilDescripcionArticulo;

    TextInputEditText etNombre;
    TextInputEditText etDescripcion;

    String pathImagen;

    Button btnPublicar;

    ImageButton ibtnSubirImagen;
    ImageButton ibtnRegresar;
    ImageButton ibtnEliminarImagen;

    public TextInputLayout getTilNombreArticulo() {
        return tilNombreArticulo;
    }

    public TextInputLayout getTilDescripcionArticulo() {
        return tilDescripcionArticulo;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nuevo_articulo);

        inicializar();


    }



    private void inicializar(){
        presenter=new NuevoArticuloPresenter(this);



        tilNombreArticulo=findViewById(R.id.til_nombre_articulo);
        tilDescripcionArticulo=findViewById(R.id.til_descripcion_articulo);

        etNombre=findViewById(R.id.et_nombre_articulo);
        etDescripcion=findViewById(R.id.et_descripcion_articulo);



        ibtnRegresar =findViewById(R.id.btn_Regresar);
        ibtnRegresar.setOnClickListener(v -> {
            cambiarActividad(Articulos.class);
        });

        ibtnSubirImagen =findViewById(R.id.ibtn_subirImagen);
        ibtnSubirImagen.setOnClickListener(v -> {

            cambiarActividad(SubirImagen.class);
        });

        ibtnEliminarImagen=findViewById(R.id.ibtn_eliminarImagen);
        ibtnEliminarImagen.setOnClickListener(v -> {
            ibtnSubirImagen.setImageBitmap(null);
            int imageResource = getResources().getIdentifier("@mipmap/default_image", null, getPackageName());
            Drawable imagen = ContextCompat.getDrawable(getApplicationContext(), imageResource);
            ibtnSubirImagen.setBackground(imagen);

            pathImagen=null;
        });

        recibirDatos();

        btnPublicar=findViewById(R.id.btn_publicar);
        btnPublicar.setOnClickListener(v -> {
            tilNombreArticulo.setError("");
            tilDescripcionArticulo.setError("");
            presenter.publicar();

        });

    }


    private void recibirDatos() {
        Bundle extras=getIntent().getExtras();
        if (extras!=null){
            String pathString=extras.getString("path");
            Bundle bundle=extras.getBundle("bundle");
            if (pathString!=null){
                ibtnSubirImagen.setImageURI(Uri.parse(pathString));
                ibtnSubirImagen.setBackground(null);
                Bitmap mapa=((BitmapDrawable) ibtnSubirImagen.getDrawable()).getBitmap();
                pathImagen=presenter.convertirBitmapAString(mapa);

            }
            if (bundle!=null){
                ibtnSubirImagen.setBackground(null);
                Bitmap bitmap=(Bitmap) bundle.get("data");
                ibtnSubirImagen.setImageBitmap(bitmap);

                pathImagen=presenter.convertirBitmapAString(bitmap);
            }
        }
    }


    @Override
    public NuevoArticuloInfo getNuevoArticuloInfo() {
        return new NuevoArticuloInfo(etNombre.getText().toString().trim(),pathImagen ,etDescripcion.getText().toString().trim());
    }

    @Override
    public void cerrar() {
        finish();
    }

    @Override
    public void cambiarActividad(Class<? extends Activity> type) {
        startActivity(new Intent(this,type));
    }

    @Override
    public void mostarMensaje(String mensaje){
        Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void nombreIncorrecto(String mensaje) {
        tilNombreArticulo.setError(mensaje);
    }

    @Override
    public void descripcionIncorrecta(String mensaje) {
        tilDescripcionArticulo.setError(mensaje);
    }

    @Override
    public void mensajeError(String mensaje) {
        Snackbar.make(btnPublicar,mensaje,Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState!=null){
            etNombre.setText(savedInstanceState.getString(Nombre));
            etDescripcion.setText(savedInstanceState.getString(Descripcion));
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState, @NonNull PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        outPersistentState.putString(Nombre,etNombre.getText().toString());
        outPersistentState.putString(Descripcion,etDescripcion.getText().toString());
    }

}