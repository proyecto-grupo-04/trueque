package com.proyectomintic.truequekapp.view.NuevoArticulo;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import com.google.android.material.snackbar.Snackbar;
import com.proyectomintic.truequekapp.model.repository.ArticuloRepository;
import com.proyectomintic.truequekapp.view.Articulos.Articulos;
import com.proyectomintic.truequekapp.view.SubirImagen.SubirImagen;

import java.io.ByteArrayOutputStream;

public class NuevoArticuloPresenter implements NuevoArticuloMVP.Presenter{
    private final NuevoArticuloMVP.View view;
    private final NuevoArticuloMVP.Model modelo;

    public NuevoArticuloPresenter(NuevoArticuloMVP.View view) {
        this.view = view;
        this.modelo=new ArticuloRepository(view.getApplicationContext());
    }



    @Override
    public void publicar() {
        NuevoArticuloInfo info=view.getNuevoArticuloInfo();

        if (info.getNombre().isEmpty()){
            view.nombreIncorrecto("El nombre del articulo no puede estar vacío");
            return;
        }else if (info.getNombre().length()>20){
            view.nombreIncorrecto("El nombre del articulo no tener mas de 20 caracteres");
            return;
        }

        if (info.getDescripcion().isEmpty()){
            view.descripcionIncorrecta("Escribe en la descripcion lo que quieres pedir a cambio");
            return;
        }
        if (info.getPath()==null || info.getPath().isEmpty() ){
            view.mostarMensaje("Debes agregar una imagen para publicar");
            return;
        }

        modelo.guardarNuevoArticulo(info);

        view.cambiarActividad(Articulos.class);
    }

    @Override
    public String convertirBitmapAString(Bitmap bitmap) {

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 40, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();

        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }
}
