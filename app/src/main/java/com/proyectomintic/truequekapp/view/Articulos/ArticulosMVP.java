package com.proyectomintic.truequekapp.view.Articulos;

import android.app.Activity;
import android.content.Context;

import java.util.List;
import java.util.Map;

public interface ArticulosMVP {

    interface Model{
        List<CargarInfo> obtenerArticulos();
        CargarInfo obtenerArticulo(Integer id);
    }
    interface Presenter{
        void cargarArticulos();
        void nuevoArticulo();
        void abrirArticulo(CargarInfo info);

    }
    interface View{
        Context getApplicationContext();
        void cambiarActividad(Class<? extends Activity> type, Map<String, String> parametros);
        void cargarArticulos(List<CargarInfo> articulos);
    }
}
