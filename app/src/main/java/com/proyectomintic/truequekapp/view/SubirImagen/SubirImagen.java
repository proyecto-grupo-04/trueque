package com.proyectomintic.truequekapp.view.SubirImagen;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;

import com.proyectomintic.truequekapp.R;
import com.proyectomintic.truequekapp.view.NuevoArticulo.NuevoArticulo;

public class SubirImagen extends Activity implements SubirImagenMVP.View{

    DisplayMetrics dm;
    int width;
    int height;
    ImageButton btnCerrar;
    Button btnSubirImagen;
    Button btnTomarFoto;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subir_imagen);

        dm=new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        width=dm.widthPixels;
        height=dm.heightPixels;

        getWindow().setLayout((int)(width*.8),(int)(height*.2));

        WindowManager.LayoutParams parametros= getWindow().getAttributes();
        parametros.gravity= Gravity.CENTER;
        parametros.x=0;
        parametros.y=0;

        getWindow().setAttributes(parametros);

        inicializar();

    }

    @Override
    public void cerrar() {
        finish();
    }

    private void inicializar(){
        btnCerrar=findViewById(R.id.btn_cerrar);
        btnCerrar.setOnClickListener(v -> {
            cambiarActividad(NuevoArticulo.class);
        });

        btnSubirImagen=findViewById(R.id.btn_desdedispositivo);
        btnSubirImagen.setOnClickListener(v -> {
            Intent capturarImagenGaleria=new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            capturarImagenGaleria.setType("image/");
            startActivityForResult(capturarImagenGaleria.createChooser(capturarImagenGaleria,"Seleccione la aplicacion"),2);
        });

        btnTomarFoto=findViewById(R.id.btn_tomarfoto);
        btnTomarFoto.setOnClickListener(v -> {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if(intent.resolveActivity(getPackageManager()) != null){
                startActivityForResult(intent, 1);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode==RESULT_OK && requestCode==2){
            String path=data.getData().toString();
            enviarImagenPath(path);
        }
        if (resultCode==RESULT_OK && requestCode==1){
            Bundle extras = data.getExtras();
            enviarImagenBundle(extras);
        }
    }

    private void enviarImagenBundle(Bundle extras) {
        Intent enviarBundleIntento=new Intent(SubirImagen.this, NuevoArticulo.class);
        enviarBundleIntento.putExtra("bundle",extras);
        startActivity(enviarBundleIntento);
    }

    private void enviarImagenPath(String path) {
        Intent enviarIntento=new Intent(SubirImagen.this, NuevoArticulo.class);
        enviarIntento.putExtra("path",path);
        startActivity(enviarIntento);
    }

    public void cambiarActividad(Class<? extends Activity> type) {
        startActivity(new Intent(this,type));
    }
}