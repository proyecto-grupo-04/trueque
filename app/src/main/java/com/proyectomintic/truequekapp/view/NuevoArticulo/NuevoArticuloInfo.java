package com.proyectomintic.truequekapp.view.NuevoArticulo;

public class NuevoArticuloInfo {
    private String Nombre;
    private String path;
    private String Descripcion;

    public NuevoArticuloInfo(String nombre, String path, String descripcion) {
        Nombre = nombre;
        this.path = path;
        Descripcion = descripcion;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }
}
