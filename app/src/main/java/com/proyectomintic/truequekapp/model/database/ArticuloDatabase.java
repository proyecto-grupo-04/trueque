package com.proyectomintic.truequekapp.model.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.proyectomintic.truequekapp.model.dao.ArticuloDao;
import com.proyectomintic.truequekapp.model.entity.Articulo;

@Database(entities = {Articulo.class},version = 1)
public abstract class ArticuloDatabase  extends RoomDatabase {
    public abstract ArticuloDao articuloDao();

    private static transient ArticuloDatabase instance=null;

    public static ArticuloDatabase obtenerInstancia(Context contexto){
        if (instance==null){
            instance= Room.databaseBuilder(contexto,ArticuloDatabase.class,"articulo")
                    .allowMainThreadQueries()
                    .build();
        }
        return instance;
    }

}
