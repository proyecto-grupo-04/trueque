package com.proyectomintic.truequekapp.model.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Articulo {
    @PrimaryKey
    private Integer id;
    private String Nombre;
    private String path;
    private String Descripcion;
    private Boolean Aceptado;

    public Boolean getAceptado() {
        return Aceptado;
    }

    public void setAceptado(Boolean aceptado) {
        Aceptado = aceptado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }
}
