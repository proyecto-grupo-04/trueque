package com.proyectomintic.truequekapp.model.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.proyectomintic.truequekapp.model.entity.Articulo;

import java.util.List;

@Dao
public interface ArticuloDao {

    @Insert
    void insert(Articulo articulo);

    @Query("SELECT * FROM articulo ORDER BY id DESC")
    List<Articulo> obtenerTodosLosArticulos();

    @Query("UPDATE articulo SET Aceptado=:Decision WHERE Nombre=:Nombre")
    void AceptarONo(String Nombre,Boolean Decision);

    @Query("SELECT * FROM articulo WHERE id=:id")
    Articulo obtenerArticulo(Integer id);
}
