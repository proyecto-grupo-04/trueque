package com.proyectomintic.truequekapp.model.repository;

import android.content.Context;

import com.proyectomintic.truequekapp.model.dao.ArticuloDao;
import com.proyectomintic.truequekapp.model.database.ArticuloDatabase;
import com.proyectomintic.truequekapp.model.entity.Articulo;
import com.proyectomintic.truequekapp.view.Articulos.ArticulosMVP;
import com.proyectomintic.truequekapp.view.Articulos.CargarInfo;
import com.proyectomintic.truequekapp.view.Check.CheckMVP;
import com.proyectomintic.truequekapp.view.NuevoArticulo.NuevoArticuloInfo;
import com.proyectomintic.truequekapp.view.NuevoArticulo.NuevoArticuloMVP;

import java.util.ArrayList;
import java.util.List;

public class ArticuloRepository implements NuevoArticuloMVP.Model, ArticulosMVP.Model, CheckMVP.Model {
    private final ArticuloDao articuloDao;

    public ArticuloRepository(Context contexto){
        ArticuloDatabase db=ArticuloDatabase.obtenerInstancia(contexto);
        articuloDao=db.articuloDao();
    }

    @Override
    public void guardarNuevoArticulo(NuevoArticuloInfo articulo) {
        Articulo articuloEntity=new Articulo();
        articuloEntity.setNombre(articulo.getNombre());
        articuloEntity.setDescripcion(articulo.getDescripcion());
        articuloEntity.setPath(articulo.getPath());

        articuloDao.insert(articuloEntity);
    }


    @Override
    public List<CargarInfo> obtenerArticulos() {
        List<Articulo> articulos=articuloDao.obtenerTodosLosArticulos();
        List<CargarInfo> respuesta=new ArrayList<>();

        for (Articulo articulo:articulos){
            respuesta.add(new CargarInfo(articulo.getId(),articulo.getNombre(),articulo.getPath(),articulo.getDescripcion(),articulo.getAceptado()));
        }
        return respuesta;
    }

    @Override
    public CargarInfo obtenerArticulo(Integer id) {
        Articulo articulo=articuloDao.obtenerArticulo(id);
        CargarInfo info=new CargarInfo(articulo.getId(), articulo.getNombre(), articulo.getPath(), articulo.getDescripcion(), articulo.getAceptado());
        return info;
    }

    @Override
    public void AceptarONo(String Nombre, Boolean decision){
        articuloDao.AceptarONo(Nombre,decision);
    }

}
